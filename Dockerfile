FROM registry.gitlab.com/security-products/container-scanning

USER root

ARG ORAS_VERSION="1.1.0"
RUN apt-get update -y && \
    apt-get install curl -y && \
    curl -LO "https://github.com/oras-project/oras/releases/download/v${ORAS_VERSION}/oras_${ORAS_VERSION}_linux_amd64.tar.gz" && \
    mkdir -p oras-install/ && \
    tar -zxf oras_${ORAS_VERSION}_*.tar.gz -C oras-install/ && \
    mv oras-install/oras /usr/local/bin/ && \   
    rm -rf oras_${ORAS_VERSION}_*.tar.gz oras-install/

## pulls to /home/gitlab
RUN oras pull ghcr.io/aquasecurity/trivy-java-db:1

USER gitlab
WORKDIR /home/gitlab
CMD ["gtcs", "scan"]


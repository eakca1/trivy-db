## Trivy Db

This is a workaround example for air-gapped environments using Container Scanner.

The setup updated default container scanner image with Trivy Java DB in build stage. Test stage in .gitlab-ci.yml is an example, how in an air-gapped setup the trivy java db can be pushed to GitLab Registry as OCI package and used within the scan